# Purpose:
#   Find MODIS granules based on input geolocations (longitude and latitude).
#
# by Hong Chen (me@hongchen.cz)
#
# Tested on macOS v10.12.6 with
#   - Python v3.6.0

import os
import sys
import glob
import datetime
import numpy as np
from pyhdf.SD import SD, SDC
from scipy import interpolate
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.path as mpl_path
from matplotlib.ticker import FixedLocator
from matplotlib import rcParams
import matplotlib.patches as patches
import cartopy.crs as ccrs




def READ_GEOMETA(date, satID='aqua', fdir='data/geoMeta/6'):

    """
    output:
        GeoMeta data of MODIS that contains the input points, e.g. flight track.

        data['GranuleID'].decode('UTF-8') to get the file name of MODIS granule
        data['StartDateTime'].decode('UTF-8') to get the time stamp of MODIS granule

        GranuleID
        StartDateTime
        ArchiveSet
        OrbitNumber
        DayNightFlag
        EastBoundingCoord
        NorthBoundingCoord
        SouthBoundingCoord
        WestBoundingCoord
        GRingLongitude1
        GRingLongitude2
        GRingLongitude3
        GRingLongitude4
        GRingLatitude1
        GRingLatitude2
        GRingLatitude3
        GRingLatitude4
    """

    fdir = '%s/%s/%4.4d' % (fdir, satID.upper(), date.year)

    fnames = sorted(glob.glob('%s/*%s*.txt' % (fdir, datetime.datetime.strftime(date, '%Y-%m-%d'))))
    if len(fnames) == 0:
        exit('Error [READ_GEOMETA]: cannot find file under %s for %s.' % (fdir, str(date)))
    elif len(fnames) > 1:
        print('Warning [READ_GEOMETA]: find more than 1 file under %s for %s.' % (fdir, str(date)))

    fname = fnames[0]

    with open(fname, 'r') as f:
        header = f.readline()

    header = header.replace('#', '').split('\\n')
    vnames = header[-1].strip().split(',')
    dtype  =  []
    for vname in vnames:
        if vname == 'GranuleID':
            form = (vname, '|S41')
            dtype.append(form)
        elif vname == 'StartDateTime':
            form = (vname, '|S16')
            dtype.append(form)
        elif vname == 'ArchiveSet':
            form = (vname, '<i4')
            dtype.append(form)
        elif vname == 'DayNightFlag':
            form = (vname, '|S1')
            dtype.append(form)
        else:
            form = (vname, '<f8')
            dtype.append(form)

    # variable names can be found under data.dtype.names
    data = np.genfromtxt(fname, delimiter=',', skip_header=1, names=vnames, dtype=dtype)

    return data





def FIND_MODIS(date, tmhr, lon, lat, satID='aqua', percentIn_threshold=0.0, tmhr_range=None):

    """
    Input:
        date: Python datetime.datetime object
        tmhr: time in hour of, e.g. flight track
        lon : longitude of, e.g. flight track
        lat : latitude of, e.g. flight track

        satID: default "aqua", can also change to "terra"
        percentIn_threshold: default 0.0, threshold percentage of input points, e.g. flight track, within MODIS 5min granule
        tmhr_range: default None, can be set using Python list, e.g. [tmhr0, tmhr1], to define time range

    output:
        GeoMeta data of MODIS that contains the input points, e.g. flight track.

        data['GranuleID'].decode('UTF-8') to get the file name of MODIS granule
        data['StartDateTime'].decode('UTF-8') to get the time stamp of MODIS granule
    """

    lon[lon>180.0] -= 360.0
    logic  = (tmhr>=0.0)&(tmhr<48.0) & (lon>=-180.0)&(lon<=180.0) & (lat>=-90.0)&(lat<=90.0)

    tmhr   = tmhr[logic]
    lon    = lon[logic]
    lat    = lat[logic]

    data = READ_GEOMETA(date, satID=satID)

    # calculate tmhr (time in hour) from MODIS time stamp
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Ndata = data.size
    tmhr_modis = np.zeros(Ndata, dtype=np.float32)
    for i in range(Ndata):
        tmhr_modis[i] = (datetime.datetime.strptime(data['StartDateTime'][i].decode('UTF-8'), '%Y-%m-%d %H:%M') - date).total_seconds() / 3600.0
    # ---------------------------------------------------------------------

    # find data within given/default time range
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if tmhr_range != None:
        try:
            indices = np.where((tmhr_modis>=tmhr_range[0]) & (tmhr_modis<tmhr_range[1]))[0]
        except IndexError:
            indices = np.where((tmhr_modis>=tmhr.min()) & (tmhr_modis<=tmhr.max()))[0]
    else:
        indices = np.arange(Ndata)
    # ---------------------------------------------------------------------

    # loop through all the "MODIS granules" constructed through four corner points
    # and find which granules contain the input data
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    proj_ori = ccrs.PlateCarree()
    indices_find   = []

    # the longitude in GeoMeta dataset is in the range of [-180, 180]
    for i, index in enumerate(indices):

        line = data[index]
        xx0  = np.array([line['GRingLongitude1'], line['GRingLongitude2'], line['GRingLongitude3'], line['GRingLongitude4'], line['GRingLongitude1']])
        yy0  = np.array([line['GRingLatitude1'] , line['GRingLatitude2'] , line['GRingLatitude3'] , line['GRingLatitude4'] , line['GRingLatitude1']])

        if (abs(xx0[0]-xx0[1])>180.0) | (abs(xx0[0]-xx0[2])>180.0) | \
           (abs(xx0[0]-xx0[3])>180.0) | (abs(xx0[1]-xx0[2])>180.0) | \
           (abs(xx0[1]-xx0[3])>180.0) | (abs(xx0[2]-xx0[3])>180.0):

            xx0[xx0<0.0] += 360.0

        # roughly determine the center of granule
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        xx = xx0[:-1]
        yy = yy0[:-1]
        center_lon = xx.mean()
        center_lat = yy.mean()
        # ---------------------------------------------------------------------

        # find a more precise center point of MODIS granule
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        proj_tmp   = ccrs.Orthographic(central_longitude=center_lon, central_latitude=center_lat)
        LonLat_tmp = proj_tmp.transform_points(proj_ori, xx, yy)[:, [0, 1]]
        center_xx  = LonLat_tmp[:, 0].mean(); center_yy = LonLat_tmp[:, 1].mean()
        center_lon, center_lat = proj_ori.transform_point(center_xx, center_yy, proj_tmp)
        # ---------------------------------------------------------------------

        proj_new = ccrs.Orthographic(central_longitude=center_lon, central_latitude=center_lat)
        LonLat_in = proj_new.transform_points(proj_ori, lon, lat)[:, [0, 1]]
        LonLat_modis  = proj_new.transform_points(proj_ori, xx0, yy0)[:, [0, 1]]

        modis_granule  = mpl_path.Path(LonLat_modis, closed=True)
        pointsIn       = modis_granule.contains_points(LonLat_in)
        percentIn      = float(pointsIn.sum()) / float(pointsIn.size) * 100.0
        if (percentIn > percentIn_threshold):
            indices_find.append(index)
    # ---------------------------------------------------------------------

    return data[indices_find]





def EARTH_VIEW(data, tmhr, lon, lat):

    """
    Purpose:
        Plot input geo info and MODIS granule on map (globe).

    input:
        data: geoMeta data

        tmhr: -
        lon : --> input geo info, e.g., flight track
        lat : -
    """

    lon[lon>180.0] -= 360.0
    logic  = (tmhr>=0.0)&(tmhr<48.0) & (lon>=-180.0)&(lon<=180.0) & (lat>=-90.0)&(lat<=90.0)

    tmhr   = tmhr[logic]
    lon    = lon[logic]
    lat    = lat[logic]

    rcParams['font.size'] = 8.0

    proj_ori = ccrs.PlateCarree()
    for i, line in enumerate(data):

        xx0  = np.array([line['GRingLongitude1'], line['GRingLongitude2'], line['GRingLongitude3'], line['GRingLongitude4'], line['GRingLongitude1']])
        yy0  = np.array([line['GRingLatitude1'] , line['GRingLatitude2'] , line['GRingLatitude3'] , line['GRingLatitude4'] , line['GRingLatitude1']])

        if (abs(xx0[0]-xx0[1])>180.0) | (abs(xx0[0]-xx0[2])>180.0) | \
           (abs(xx0[0]-xx0[3])>180.0) | (abs(xx0[1]-xx0[2])>180.0) | \
           (abs(xx0[1]-xx0[3])>180.0) | (abs(xx0[2]-xx0[3])>180.0):

            xx0[xx0<0.0] += 360.0

        xx = xx0[:-1]
        yy = yy0[:-1]
        center_lon = xx.mean()
        center_lat = yy.mean()

        # second attempt to find the center point of MODIS granule
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        proj_tmp   = ccrs.Orthographic(central_longitude=center_lon, central_latitude=center_lat)
        LonLat_tmp = proj_tmp.transform_points(proj_ori, xx, yy)[:, [0, 1]]
        center_xx  = LonLat_tmp[:, 0].mean(); center_yy = LonLat_tmp[:, 1].mean()
        center_lon, center_lat = proj_ori.transform_point(center_xx, center_yy, proj_tmp)
        # ---------------------------------------------------------------------

        proj_new = ccrs.Orthographic(central_longitude=center_lon, central_latitude=center_lat)
        LonLat_in = proj_new.transform_points(proj_ori, lon, lat)[:, [0, 1]]
        LonLat_modis  = proj_new.transform_points(proj_ori, xx0, yy0)[:, [0, 1]]

        ax = plt.axes(projection=proj_new)
        ax.set_global()
        ax.stock_img()
        ax.coastlines(color='gray', lw=0.2)
        # title = RENAME_MODIS(data[i]['GranuleID'].decode('UTF-8'))
        title = data[i]['GranuleID'].decode('UTF-8')
        ax.set_title(title, fontsize=8)

        modis_granule  = mpl_path.Path(LonLat_modis, closed=True)
        pointsIn       = modis_granule.contains_points(LonLat_in)
        percentIn      = float(pointsIn.sum()) / float(pointsIn.size) * 100.0
        if (percentIn > 0):
            patch = patches.PathPatch(modis_granule, facecolor='g', edgecolor='g', alpha=0.4, lw=0.2)
        else:
            patch = patches.PathPatch(modis_granule, facecolor='k', edgecolor='k', alpha=0.2, lw=0.2)

        cs = ax.scatter(lon, lat, transform=ccrs.Geodetic(), s=0.01, c=tmhr, cmap='jet')
        ax.scatter(xx.mean(), yy.mean(), marker='*', transform=ccrs.Geodetic(), s=6, c='r')
        ax.scatter(center_lon, center_lat, marker='*', transform=ccrs.Geodetic(), s=6, c='b')
        ax.add_patch(patch)
        plt.colorbar(cs, shrink=0.6)
        plt.savefig('%s.png' % '.'.join(title.split('.')[:-1]))
        plt.close()

    # ---------------------------------------------------------------------






def RENAME_MODIS(filename):

    """
    Purpose:
        Change the "Day-Of-Year" in the MODIS file name to "YearMonthDay"

    input:
        MODIS file name, e.g. MYD03.A2014290.0035.006.2014290162522.hdf
    output:
        new MODIS file name, e.g., MYD03.A20141017.0035.006.2014290162522.hdf
    """

    try:
        fwords = filename.split('.')
        date = datetime.datetime.strptime(fwords[1], 'A%Y%j')
        fwords[1] = date.strftime('A%Y%m%d')
        return '.'.join(fwords)

    except ValueError:
        print('Warning [RENAME_MODIS]: cannot convert, return input filename as new filename.')
        return filename






if __name__ == '__main__':

    pass
