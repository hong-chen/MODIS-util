import os
import glob
import h5py
import struct
import numpy as np
import datetime
from scipy.io import readsav
from scipy import stats
from scipy import interpolate
from scipy.optimize import curve_fit
import pysolar
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator
from matplotlib import rcParams
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import cartopy.crs as ccrs




class READ_ICT_HSK:

    def __init__(self, fname, tmhr_range=None):

        f = open(fname, 'r')
        firstLine = f.readline()
        skip_header = int(firstLine.split(',')[0])

        vnames = []
        units  = []
        for i in range(7):
            f.readline()

        line = f.readline()
        vname0, unit0 = self.VARIABLE_INFO(line)
        vnames.append(vname0)
        units.append(unit0)
        Nvar = int(f.readline())
        for i in range(2):
            f.readline()
        for i in range(Nvar):
            line = f.readline()
            vname0, unit0 = self.VARIABLE_INFO(line)
            vnames.append(vname0)
            units.append(unit0)
        f.close()

        data = np.genfromtxt(fname, skip_header=skip_header, delimiter=',')
        self.data = {}

        if tmhr_range != None:
            tmhr0 = data[:, 0]/3600.0
            logic = (tmhr0>=tmhr_range[0]) & (tmhr0<=tmhr_range[1])
            for i, vname in enumerate(vnames):
                self.data[vname] = data[:, i][logic]
        else:
            for i, vname in enumerate(vnames):
                self.data[vname] = data[:, i]

    def VARIABLE_INFO(self, line):

        words = line.split(',')
        vname = words[0].strip()
        unit  = ','.join(words[1:])
        return vname, unit





def PRH2ZA(ang_pit, ang_rol, ang_head, is_rad=False):

    """
    input:
    ang_pit   (Pitch)   [deg]: positive (+) values indicate nose up
    ang_rol   (Roll)    [deg]: positive (+) values indicate right wing down
    ang_head  (Heading) [deg]: positive (+) values clockwise, w.r.t. north

    "vec": normal vector of the surface of the sensor

    return:
    ang_zenith : angle of "vec" [deg]
    ang_azimuth: angle of "vec" [deg]: positive (+) values clockwise, w.r.t. north
    """

    if not is_rad:
        rad_pit  = np.deg2rad(ang_pit)
        rad_rol  = np.deg2rad(ang_rol)
        rad_head = np.deg2rad(ang_head)

    uz =  np.cos(rad_rol)*np.cos(rad_pit)
    ux =  np.sin(rad_rol)
    uy = -np.cos(rad_rol)*np.sin(rad_pit)

    vz = uz.copy()
    vx = ux*np.cos(rad_head) + uy*np.sin(rad_head)
    vy = uy*np.cos(rad_head) - ux*np.sin(rad_head)

    ang_zenith  = np.rad2deg(np.arccos(vz))
    ang_azimuth = np.rad2deg(np.arctan2(vx,vy))

    ang_azimuth[ang_azimuth<0.0] += 360.0

    return ang_zenith, ang_azimuth






def MUSLOPE(sza, saa, iza, iaa, is_rad=False):

    if not is_rad:
        rad_sza = np.deg2rad(sza)
        rad_saa = np.deg2rad(saa)
        rad_iza = np.deg2rad(iza)
        rad_iaa = np.deg2rad(iaa)

    zs = np.cos(rad_sza)
    ys = np.sin(rad_sza) * np.cos(rad_saa)
    xs = np.sin(rad_sza) * np.sin(rad_saa)

    zi = np.cos(rad_iza)
    yi = np.sin(rad_iza) * np.cos(rad_iaa)
    xi = np.sin(rad_iza) * np.sin(rad_iaa)

    mu = xs*xi + ys*yi + zs*zi

    return mu






def CAL_SOLAR_ANGLES(julian_day, longitude, latitude, altitude):

    dateRef = datetime.datetime(1, 1, 1)
    jdayRef = 1.0

    sza = np.zeros_like(julian_day)
    saa = np.zeros_like(julian_day)

    for i, jday in enumerate(julian_day):

        dtime_i = (dateRef + datetime.timedelta(days=jday-jdayRef)).replace(tzinfo=datetime.timezone.utc)

        sza_i = 90.0 - pysolar.solar.get_altitude(latitude[i], longitude[i], dtime_i, elevation=altitude[i])
        if sza_i < 0.0 or sza_i > 90.0:
            sza_i = np.nan
        sza[i] = sza_i

        saa_i = pysolar.solar.get_azimuth(latitude[i], longitude[i], dtime_i, elevation=altitude[i])
        if saa_i >= 0.0:
            if 0.0<=saa_i<=180.0:
                saa_i = 180.0 - saa_i
            elif 180.0<saa_i<=360.0:
                saa_i = 540.0 - saa_i
            else:
                saa_i = np.nan
        elif saa_i < 0.0:
            if -180.0<=saa_i<0.0:
                saa_i = -saa_i + 180.0
            elif -360.0<=saa_i<-180.0:
                saa_i = -saa_i - 180.0
            else:
                saa_i = np.nan
        saa[i] = saa_i

    return sza, saa



if __name__ == '__main__':

    pass
