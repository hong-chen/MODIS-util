"""
Sebastian Schmidt / Hong Chen, 2017/2018
"""
import numpy as np
import math
from sys import exit as ext
import os.path
import h5py
from pyhdf.SD import SD, SDC

"""
Read MODIS L1B quarter / half km (02QKM, 02HKM) data with geolocation information:
    QKM(MxD_02QKM_file,radiance=False,reflectance=False,verbose=False)

    Usage example: radiance,longitude,latitude=QKM('l1b.hdf',radiance=True)
    Call options : radiance    = True: return radiance instead of counts
                   reflectance = True: return reflectance instead of counts 
"""
class MODISL1B:
    def __init__(self,file,reflectance=True,radiance=False,thermal=False,verbose=False):
        # find out whether it's QKM or HKM (quarter or half kilometer data)
        fn  = filename(file)
        ph  = fn.find('02HKM')
        pq  = fn.find('02QKM')
        pf  = fn.find('021KM')
        sav = file+'.sav' # name of save file to be created
        #self.sav=sav # Path of save file
        if( ph<0 and pq <0 and pf<0): ext('This is not a valid file')
        if( ph>1 and pq >1 and pf>1): ext('Could not identify file type')
        if ph > 0: HKM=True
        else: HKM=False
        if pq > 0: QKM=True
        else: QKM=False
        if pf > 0: FKM=True
        else: FKM=False

        channels_qkm=[650,860]                  # 1,2
        channels_hkm=[470,555,1240,1640,2130]   # 3,4,5,6,7
        # RGB: 1,4,3
        # cloud composite: 3,6,7 (most useful for ice cloud identification)
        # more channel combinations: https://earthdata.nasa.gov/faq#ed-rapid-response-faq

        # restore save file or read it from the HDF
        if os.path.isfile(sav):
            if verbose: print("Restore "+sav)
            self.restore_l1b(sav)
        else:
            if verbose: print("Read "+file)
            handle = SD(file, SDC.READ)
            # Get data
            sds = handle.select('Latitude') # select sds
            lat = sds.get() # get sds data
            sds = handle.select('Longitude') # select sds
            lon = sds.get() # get sds data
            if HKM: sds = handle.select('EV_500_RefSB') # select sds
            if QKM: sds = handle.select('EV_250_RefSB') # select sds
            if FKM: sds = handle.select('EV_1KM_RefSB') # select sds
            ref = np.array(sds.get()) # get sds data
            ref = np.float_(ref)
            ro=sds.attributes()['corrected_counts_offsets']
            rs=sds.attributes()['corrected_counts_scales']
            if verbose:
                print("latitude    dim:",lat.shape)
                print("reflectance dim:",ref.shape)
            nx=ref.shape[1]
            ny=ref.shape[2]

            what='counts'
            if radiance:
                what='radiance'
                ro=np.array(sds.attributes()['radiance_offsets'])
                rs=np.array(sds.attributes()['radiance_scales'])
            if reflectance:
                what='reflectance'
                ro=np.array(sds.attributes()['reflectance_offsets'])
                rs=np.array(sds.attributes()['reflectance_scales'])
                if radiance: print("Warning: Cannot get radiance and reflectance simultaneously.")
            if verbose:
                print('Now rescaling both channels.')
                print('Offsets:',ro)
                print('Scales :',rs)
            rc=ref.copy()
            ro=np.float_(ro)
            rs=np.float_(rs)
            for i in range(2):
                rc[i,:,:]=ro[i]+rs[i]*np.float_(ref[i,:,:])

            if FKM and thermal: sds = handle.select('EV_1KM_Emissive') # select sds
            thr  = np.array(sds.get()) # get sds data   # thermal radiance
            tro  = sds.attributes()['radiance_offsets']
            trs  = sds.attributes()['radiance_scales']
            tc   = sds.attributes()['band_names']
            print(tc)
            print(type(tc))

            thrs = thr.copy()
            for i in range(2):
                thrs[i,:,:]=tro[i]+trs[i]*np.float_(thr[i,:,:]) # correct scale applied
            handle.end()

            # grid interpolation, following
            # http://www.icare.univ-lille1.fr/wiki/index.php/MODIS_geolocation
            if HKM:
                if verbose:print('Now interpolating lat')
                lath=modis_geo_interp_500(lat)
                if verbose:print('Now interpolating lon')
                lonh=modis_geo_interp_500(lon)
                if verbose:print('Shape:',lonh.shape)
                channels=channels_hkm
            if QKM:
                if verbose:print('Now interpolating lat')
                lath=modis_geo_interp_250(lat)
                if verbose:print('Now interpolating lon')
                lonh=modis_geo_interp_250(lon)
                if verbose:print('Shape:',lonh.shape)
                channels=channels_qkm
            if FKM:
                # note: 13 & 14 have a "hi" and "lo" component
                # ocean color channels
                # 8,9,10,11,12,13hi,13lo,14hi,14lo,15,16,
                # water vapor channels
                # 17,18,19
                # SW cirrus
                # 26
                channels=[421.5,443,488,531,551,667,667,678,678,748,869.5,\
                         905,936,940,\
                         1375]
                lath=np.zeros([nx,ny])
                lonh=np.zeros([nx,ny])
                for i in range(nx):
                    for j in range(ny):
                        lath[i,j], lonh[i,j] = get_1km_pix_pos ( i, j, lat, lon)

            if what == 'counts'     : self.counts      = rc
            if what == 'radiance'   : self.radiance    = rc
            if what == 'reflectance': self.reflectance = rc
            if thermal              : self.thermal     = thrs
            if thermal              : self.tchannels   = tc
            self.channels=channels
            self.lon=lonh
            self.lat=lath

            self.save_l1b(sav)


    def restore_l1b(self,file):
        mod              = h5py.File(file, "r")
        self.lon         = mod["lon"][...]
        self.lat         = mod["lat"][...]
        self.reflectance = mod["reflectance"][...]
        self.thermal     = mod["thermal"][...]
        self.tchannels   = mod["tchannels"][...]
        #self.wesn        = mod["wesn"][...]
        #self.sav         = str(mod["sav"][...])
        self.channels    = mod["channels"][...]
        #self.nl,self.nx,self.ny = self.reflectance.shape
        mod.close()

    def save_l1b(self,file):
        mod = h5py.File(file, "w")
        mod["lon"]         = self.lon
        mod["lat"]         = self.lat
        mod["reflectance"] = self.reflectance
        mod["thermal"]     = self.thermal
        mod["tchannels"]   = self.tchannels
        #mod["wesn"]        = self.wesn
        #mod["sav"]         = self.sav
        mod["channels"]    = self.channels
        mod.close()

    def plot_sw(self,ax,channel=0):
        ax.contourf(self.lon,self.lat,self.reflectance[channel,:,:],aspect=1)
        ax.set_title("MODIS reflectance, channel "+str(self.channels[channel]))
        ax.set_aspect(aspect=1)

    def plot_lw(self,ax,channel=0):
        ax.contourf(self.lon,self.lat,self.thermal[channel,:,:],aspect=1)
        ax.set_title("MODIS emitted radiance, channel "+str(channel))
        ax.set_aspect(aspect=1)


def bilinear(p,xnew,ynew,xold=None,yold=None,kind='linear'):
    from scipy.interpolate import interp2d
    if p.ndim == 2: # only needed for regularly gridded input arrays
        nx=p.shape[0]
        ny=p.shape[1]
    if xold is None: xold=np.arange(nx) # old x coordinates (indeces)
    if yold is None: yold=np.arange(ny) # old y coordinates (indeces)
    f=interp2d(yold,xold,p,kind=kind)
    return(f(ynew,xnew))


# Interpolate lat/lon to 250 m resolution
def modis_geo_interp_250(data):
    # Get size of input array (1000 meter resolution)
    dims  = data.shape
    ncol  = dims[0]
    nrow  = dims[1]
    nscan = nrow // 10

    # Interpolation to 250 meter resolution
    result = np.zeros((4 * ncol, 4 * nrow))
    x      = np.arange(4 * ncol) * 0.25
    y      = np.arange(40) * 0.25 - 0.375
    for scan in range(nscan+1):
        # Use bilinear interpolation for all 250 meter pixels
        j0 = 10 * scan
        j1 = 10 * scan + 10
        k0 = 40 * scan
        k1 = 40 * scan + 40
        if scan==nscan:
            if j1>nrow: j1=nrow
            if k1>nrow*4: k1=nrow*4
            result[:,k0:k1] = bilinear(data[:,j0:j1], x, y[0:k1-k0])
        else:
            result[:,k0:k1] = bilinear(data[:,j0:j1], x, y)
        # Use linear extrapolation for the first two 250 meter pixels along track
        m = (result[:, k0 + 5] - result[:, k0 + 2]) / (y[5] - y[2])
        b =  result[:, k0 + 5] - m * y[5]
        result[:, k0 + 0] = m * y[0] + b
        result[:, k0 + 1] = m * y[1] + b
        # Use linear extrapolation for the last two 250 meter pixels along track
        k34=k1-k0-6
        k37=k1-k0-3
        k38=k1-k0-2
        k39=k1-k0-1
        m = (result[:, k37] - result[:, k34]) / (y[k37] - y[k34])
        b = result[:, k37] - m * y[k37]
        result[:, k38] = m * y[k38] + b
        result[:, k39] = m * y[k39] + b
    return(result)

# Interpolate lat/lon to 500 m resolution
def modis_geo_interp_500(data):
    # Get size of input array (1000 meter resolution)
    dims  = data.shape
    ncol  = dims[0]
    nrow  = dims[1]
    nscan = nrow // 10
 
    # Interpolation to 500 meter resolution
    result = np.zeros((2 * ncol, 2 * nrow))
    x      = np.arange(2 * ncol) * 0.5
    y      = np.arange(20) * 0.5 - 0.25
    for scan in range(nscan+1): 
        # Use bilinear interpolation for all 250 meter pixels
        j0 = 10 * scan
        j1 = 10 * scan + 10
        k0 = 20 * scan
        k1 = 20 * scan + 20
        if scan==nscan: # last scan - may not be complete
            if j1>nrow: j1=nrow
            if k1>nrow*2: k1=nrow*2 
            result[:,k0:k1] = bilinear(data[:,j0:j1], x, y[0:k1-k0])
        else:
            result[:,k0:k1] = bilinear(data[:,j0:j1], x, y)

        # Use linear extrapolation for the first two 500 meter pixels along track
        m = (result[:, k0 + 2] - result[:, k0 + 1]) / (y[2] - y[1])
        b =  result[:, k0 + 2] - m * y[2]
        result[:, k0 + 0] = m * y[0] + b

        # Use linear extrapolation for the last two 500 meter pixels along track
        k18=k1-k0-2
        k17=k1-k0-3
        k19=k1-k0-1
        m = (result[:,k18] - result[:,k17]) / (y[k18] - y[k17])
        b = result[:, k18] - m * y[k18]
        result[:, k19] = m * y[k19] + b
    return(result)  

# Below is the calculation of 1km lats and lons for L1BF (1KM)
# --- 5km -> 1km ---
sz_sc_5km = 270 # number of 5km pixels along scan

def itk_5km_to_1km ( i_tk_5km ) :
    """
    return the 1km grid index along track of a 5km pixel
    """
    return 2 + 5 * i_tk_5km

def itk_1km_to_5km ( i_tk_1km ) :
    """
    return the 5km grid index along track of a 1km pixel
    """
    return ( i_tk_1km - 2. ) / 5.

def isc_5km_to_1km ( i_sc_5km ) :
    """
    return the 1km grid index cross track of a 5km pixel
    """
    return 2. + 5. * i_sc_5km

def isc_1km_to_5km ( i_sc_1km ) :
    """
    return the 5km grid index cross track of a 1km pixel
    """
    return  ( i_sc_1km - 2. ) / 5.

def get_1km_pix_pos ( itk_1km, isc_1km, lat_5km, lon_5km ) :
    """
    return the (lat,lon) of a 1km pixel specified with its indexes, when the geolocation datasets are given at 5km resolution
    @param itk_1km grid index of the 1km pixel along-track
    @param isc_1km grid index of the 1km pixel cross-track
    @param lat_5km latitudes dataset at 5km resolution
    @param lon_5km longitudes dataset at 5km resolution
    @return the ( lat, lon ) of the 1km pixel  [ itk_1km, isc_1km ]
    """
    # check 1km indexes validity
    sz_tk_5km = lat_5km.shape[0] + 1
    sz_tk_1km = 5 * sz_tk_5km
    sz_sc_1km = ( 5 * sz_sc_5km ) + 6
    if ( isc_1km < 0 ) or ( isc_1km > sz_sc_1km - 1 ) :
        raise ValueError ( "Invalid scan index %d. Must be in range [%d,%d]"%(isc_1km,0,sz_sc_1km-1) )
    if ( itk_1km < 0 ) or ( itk_1km > sz_tk_1km - 1 ) :
        raise ValueError ( "Invalid track index %d. Must be in range [%d,%d]"%(itk_1km,0,sz_tk_1km-1) )

    # --- set the bounding 5km pixels to take for interpolation
    # set the (track,scan) indexes of the 5km pixel in the 1km grid
    itk_5km = itk_1km_to_5km ( itk_1km )
    isc_5km = isc_1km_to_5km ( isc_1km )

    #print "i_1km=[%.2f, %.2f] -> i_5km=[%.2f, %.2f]"%(itk_1km, isc_1km, itk_5km, isc_5km)

    # the width of one scan, in number of pixels, at 1km resolution
    w_scan_1km = 10.
    # - extra/interpolation 5km pixels along track
    if ( itk_1km % w_scan_1km ) <= 2 : # extrapolate start of scan
        itk_top_5km    = math.ceil ( itk_5km )
        itk_bottom_5km = itk_top_5km + 1
    elif ( itk_1km % w_scan_1km ) >= 7 : # extrapolate end of scan
        itk_top_5km    = math.floor ( itk_5km )
        itk_bottom_5km = itk_top_5km - 1
    else : # general case : middle of scan
        itk_top_5km    = math.floor ( itk_5km )
        itk_bottom_5km = itk_top_5km + 1
    # - extra/interpolation 5km pixels cross track
    if ( isc_1km <= 2 ) : # extrapolate start of scan line
        isc_left_5km  = 0
        isc_right_5km = 1
    elif ( isc_5km >= ( sz_sc_5km - 1 )  ) : # extrapolate end of scan line
        isc_left_5km  = sz_sc_5km - 2
        isc_right_5km = sz_sc_5km - 1
    else : # general case : interpolation
        isc_left_5km  = math.floor ( isc_5km )
        isc_right_5km = isc_left_5km + 1

    #print "itk_top_5km=%d itk_bottom_5km=%d isc_left_5km=%d isc_right_5km=%d"%(itk_top_5km, itk_bottom_5km, isc_left_5km, isc_right_5km)

    # --- set the 5km track lines position ; left border ---
    lat_left_1km, lon_left_1km   = get_y_pos_5km_to_1km (
                isc_left_5km, itk_top_5km, itk_bottom_5km,
                lat_5km, lon_5km,
                itk_1km )
    # --- set the 5km track lines position ; right border ---
    lat_right_1km, lon_right_1km = get_y_pos_5km_to_1km (
                isc_right_5km, itk_top_5km, itk_bottom_5km,
                lat_5km, lon_5km,
                itk_1km )

    # check for change date meridian case
    if  abs ( lon_right_1km  - lon_left_1km  ) > 180. :
        # all negative longitudes will be incremented of 360 before interpolation
        if lon_left_1km < 0. :
            lon_left_1km += 360.
        if lon_right_1km < 0. :
            lon_right_1km += 360.

    # for each track line position, interpolate along scan to retrieve the 1km geolocation
    lat, lon = get_x_pos_5km_to_1km ( lat_left_1km,  lon_left_1km,  isc_left_5km,
                                     lat_right_1km, lon_right_1km, isc_right_5km,
                                     isc_1km )
    #print "geolocation = [%f, %f]"%(lat,lon)

    # in case of change date crossing, turn values > 180. to negative < -180 to positive
    if lon > 180. :
        lon = lon - 360.
    elif lon < -180. :
        lon = lon + 360.
    return lat, lon

def get_x_pos_5km_to_1km ( lat_left_1km,  lon_left_1km,  isc_left_5km,
                           lat_right_1km, lon_right_1km, isc_right_5km,
                           isc_1km
                        ) :

    # make sure P1 and P1 are 2 successive pixels along-track
    if abs ( isc_left_5km - isc_right_5km ) != 1 :
        raise ValueError ( "The 2 borders are on the same along-track line and must be successive" )

    # coordinates of left and right border in the 1km grid
    isc_left_1km  = isc_5km_to_1km ( isc_left_5km )
    isc_right_1km = isc_5km_to_1km ( isc_right_5km )

    # linear interpolation on the position
    alpha_lon = ( lon_right_1km - lon_left_1km ) / ( isc_right_1km - isc_left_1km )
    alpha_lat = ( lat_right_1km - lat_left_1km ) / ( isc_right_1km - isc_left_1km )

    lat = lat_left_1km + alpha_lat * ( isc_1km - isc_left_1km )
    lon = lon_left_1km + alpha_lon * ( isc_1km - isc_left_1km )

    return lat, lon

def get_y_pos_5km_to_1km (  isc_5km, itk_p1_5km, itk_p2_5km,
                            lat_5km, lon_5km,
                            itk_1km ) :

    # make sure P1 and P1 are 2 successive pixels along-track
    if abs ( itk_p1_5km - itk_p2_5km ) != 1 :
        raise ValueError ( "P1 and P2 are on the same cross-track line and must be successive" )

    # lat, lon of the 5km bounding pixels
    p1_5km_lat = lat_5km [ itk_p1_5km, isc_5km ]
    p1_5km_lon = lon_5km [ itk_p1_5km, isc_5km ]
    p2_5km_lat = lat_5km [ itk_p2_5km, isc_5km ]
    p2_5km_lon = lon_5km [ itk_p2_5km, isc_5km ]

    # check for change date meridian case
    if  abs ( p1_5km_lon  - p2_5km_lon  ) > 180. :
        # all negative longitudes will be incremented of 360 before interpolation
        if p1_5km_lon < 0. :
            p1_5km_lon += 360.
        if p2_5km_lon < 0. :
            p2_5km_lon += 360.

    # coordinates of p1, p2 in the 1km grid
    itk_p1_1km = itk_5km_to_1km ( itk_p1_5km )
    itk_p2_1km = itk_5km_to_1km ( itk_p2_5km )

    #print "itk_p1_1km=%f itk_p2_1km=%f"%( itk_p1_1km, itk_p2_1km )

    # linear interpolation on the position
    alpha_lon = ( p2_5km_lon - p1_5km_lon ) / ( itk_p2_1km - itk_p1_1km )
    alpha_lat = ( p2_5km_lat - p1_5km_lat ) / ( itk_p2_1km - itk_p1_1km )
    lon = p1_5km_lon + alpha_lon * ( itk_1km - itk_p1_1km )
    lat = p1_5km_lat + alpha_lat * ( itk_1km - itk_p1_1km )

    if lon > 180. :
        lon = lon - 360.
    elif lon < -180. :
        lon = lon + 360.

    return lat, lon

def filename(path):
    if type(path) is str:
        pos =path.rfind("/")
        name=path[pos+1:]
    else:
        name=None
    return(name)
        
"""
Testing:
"""
if __name__=='__main__':
    import matplotlib.pyplot as plt

    # file names
    data  = '/Users/schmidt/rtm/sari/dat/modis/'
    l1bq  = data+'MYD02QKM.A2017204.0500.006.2017204171502.hdf'
    l1bh  = data+'MYD02HKM.A2017204.0500.006.2017204171502.hdf'
    l1bf  = data+'MYD021KM.A2017204.0500.006.2017204171502.hdf'

    # prepare figure
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(11,5))

    ##################################################################
    # (1) get MODIS L1B file. Three types: qkm, hkm, fkm (quarter, half, full kilometer)
    ##################################################################
    modis=MODISL1B(l1bf,verbose=False,thermal=True) # either l1bq, l1bh, l1bf

    # Object "modis" contains the following variables:
    # modis.lat --> latitudes
    # modis.lon --> longitudes
    # modis.channels --> wavelengths
    # modis.reflectance --> reflectance at these channels
    print('Available wavelengths:',modis.channels)

    # plot routine that comes with MODIS L1B class
    modis.plot_lw(ax1,channel=0)
    modis.plot_sw(ax2,channel=0)

