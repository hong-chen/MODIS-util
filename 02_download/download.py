import os
import sys
import glob
import datetime
import urllib.request
import numpy as np
from pyhdf.SD import SD, SDC





def MODIS_HTTPS_DOWNLOAD(date, data_tags, pattern, fdir_prefix='/archive/allData/61', fdir_out='data', verbose=False, test_mode=False):

    # How to get an app_key:
    # https://ladsweb.modaps.eosdis.nasa.gov/tools-and-services/data-download-scripts/#appkeys
    # after you obtain an app_key, define a string variable
    # >app_key = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'<
    # and comment out
    # >from config import app_key<
    from config import app_key

    server  = 'https://ladsweb.modaps.eosdis.nasa.gov'

    links_wget = []
    for data_tag in data_tags:

        fdir_data = GET_FDIR(date, data_tag, fdir_prefix=fdir_prefix)
        link_fdir = server + fdir_data

        link_csv = '%s.csv' % (link_fdir)
        try:
            webpage  = urllib.request.urlopen(link_csv)
            content  = webpage.read().decode('utf-8')
            lines    = content.split('\n')
            for line in lines:
                if len(line)>30:
                    filename = line.split(',')[0]
                    if pattern in filename:
                        link_wget = '%s/%s' % (link_fdir, filename)
                        links_wget.append(link_wget)
        except:
            print('Warning [HTTPS_DOWNLOAD]: cannot access csv file list at \'%s\'.' % link_fdir)

    for link_wget in links_wget:
        print('+')
        command = 'wget --quiet --show-progress --execute robots=off --mirror --no-parent --reject .html,.tmp,.json --no-host-directories "%s" --header "Authorization: Bearer %s" --directory-prefix=%s' % (link_wget, app_key, fdir_out)
        if verbose:
            print(command)
            print()
        try:
            if not test_mode:
                os.system(command)
            fname_local = link_wget.replace(server, fdir_out)
            data_format = fname_local.split('.')[-1]
            if data_format == 'hdf':
                f = SD(fname_local, SDC.READ)
                f.end()
                print('Message [HTTPS_DOWNLOAD]: \'%s\' has been downloaded.' % fname_local)
        except:
            print('Warning [HTTPS_DOWNLOAD]: failed to download \'%s\'.' % link_wget)
        print('-')






def GET_FDIR(date, data_tag, fdir_prefix='/archive/allData/61'):

    year_tag = str(date.timetuple().tm_year).zfill(4)
    doy_tag  = str(date.timetuple().tm_yday).zfill(3)

    fdir     = '%s/%s/%s/%s' % (fdir_prefix, data_tag, year_tag, doy_tag)
    return fdir







if __name__ == '__main__':

    # for usage examples, check example.py
    pass
