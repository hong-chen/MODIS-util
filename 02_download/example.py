import os
import sys
import glob
import datetime
import multiprocessing as mp

from download import MODIS_HTTPS_DOWNLOAD




def DOWNLOAD_20140911():

    """
    This is an example code for BRDF work under
    /Users/hoch4240/Chen/work/10_Arctic/BRDF
    """

    date = datetime.datetime(2014, 9, 11)

    data_tags_terra = ['MOD02QKM', 'MOD02HKM', 'MOD021KM', 'MOD03', 'MOD06_L2']
    patterns_terra  = ['A2014254.2025', 'A2014254.2340']
    for pattern_terra in patterns_terra:
        MODIS_HTTPS_DOWNLOAD(date, data_tags_terra, pattern_terra)
    exit()

    data_tags_aqua = ['MYD02QKM', 'MYD02HKM', 'MYD021KM', 'MYD03', 'MYD06_L2']
    patterns_aqua  = ['A2014254.1550', 'A2014254.1730', 'A2014254.1905', 'A2014254.2045', 'A2014254.2220']
    for pattern_aqua in patterns_aqua:
        MODIS_HTTPS_DOWNLOAD(date, data_tags_aqua, pattern_aqua)





if __name__ == '__main__':

    DOWNLOAD_20140911()
